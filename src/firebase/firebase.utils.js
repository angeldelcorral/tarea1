import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

// config
const config = {
  apiKey: "AIzaSyBt7f17HAmhQuzFFBb-rdY4cu9RHX-9Mxk",
  authDomain: "proyecto1-f4c6c.firebaseapp.com",
  projectId: "proyecto1-f4c6c",
  storageBucket: "proyecto1-f4c6c.appspot.com",
  messagingSenderId: "55306773879",
  appId: "1:55306773879:web:01d73bd5476a4a3ab0ad72",
};

// create my web app with firebase
firebase.initializeApp(config);

// function for create-documents
export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uuid}`);

  const snapShot = await userRef.get();

  if (!snapShot.exists) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();
    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData,
      });
    } catch (error) {
      console.log("error creating user", error.message);
    }
  }
  return userRef;
};

// exports
export const auth = firebase.auth();
export const firestore = firebase.firestore();

// providers
const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: "select_account" });
export const signInWithGoogle = () => {
  auth.signInWithPopup(provider)
};

export default firebase;
