export function getNamePermissions(permission) {
  switch (permission) {
    case 1:
      return "Usuarios";
    case 2:
      return "Gestor";
    case 3:
      return "Dashboard";
    case 4:
      return "Kill Switch";
    case 5:
      return "Configuración";
    case 6:
      return "Ficha Médica";
    case 7:
      return "Reportes";
    case 8:
      return "Casos Sospechosos";
    case 9:
      return "Crear Caso Sospechoso";
    case 10:
      return "Cargar Tests PCR";
    case 11:
      return "Seguimiento COVID";
    case 12:
      return "Actualizar Datos y Vacunas";
    case 13:
      return "Panel de Control";
    case 14:
      return "Terapia Respiratoria";
    case 15:
      return "Tele-Consulta";
    default:
      break;
  }
}
