import React from "react";
import { Switch, Route } from "react-router-dom";
import { auth, createUserProfileDocument } from "./firebase/firebase.utils";
import "./App.css";

import Header from "./components/header/header.component";

import Home from "./pages/home";
import Users from "./pages/users";
import LoginPage from "./pages/login";

class App extends React.Component {
  constructor() {
    super();

    this.state = {
      currentUser: null,
    };
  }
  unsubscribeFromAuth = null;

  // life cycles: mount, updateMount, willunmount
  componentDidMount() {
    this.unsubscribeFromAuth = auth.onAuthStateChanged(async (userAuth) => {
      if (userAuth) {
        const userRef = await createUserProfileDocument(userAuth);
        userRef.onSnapshot((snapShot) => {
          this.setState({
            currentUser: {
              id: snapShot.id,
              ...snapShot.data(),
            },
          });
          console.log(this.state);
        });
      }
      this.setState({ currentUser: userAuth });
    });
  }

  componentWillUnmount() {
    this.unsubscribeFromAuth();
  }
  
  render (){
    const { currentUser } = this.state;
    // console.log('Aqui', currentUser);
    return (
      <div>
        <Header currentUser={currentUser} />
        <Switch>
          {currentUser ? (
            <Route exact path="/" component={Home}/>
          ) : (
            <Route exact path="/" component={LoginPage} />
          )}
          {/* <Route exact path="/" component={LoginPage} /> */}
          <Route path="/home" component={Home}/>
          <Route path="/users" component={Users} />
        </Switch>
      </div>
    );
  }
}

export default App;
