import React from "react";
import PropTypes from "prop-types";
import AvatarCircle from "../avatar-circle/avatar-circle";
import "./avatar.styles.css";

export default class Avatar extends React.Component {
  shouldComponentUpdate(nextProps) {
    return (
      nextProps.avatarImg !== this.props.avatarImg ||
      nextProps.infoTitle !== this.props.infoTitle ||
      nextProps.nameFallback !== this.props.nameFallback ||
      nextProps.type !== this.props.type
    );
  }
  getInfo() {
    const { infoTitle } = this.props;

    if (!infoTitle) {
      return null;
    }

    if (typeof infoTitle == "string") {
      return (
        <span className="avatar-info">
          <span className="avatar-title">{infoTitle}</span>
        </span>
      );
    } else {
      return <span className="avatar-info">{infoTitle}</span>;
    }
  }
  getAvatar() {
    const { avatarImg, nameFallback } = this.props;

    if (avatarImg) {
      return <img className="rounded-img" src={avatarImg} alt="user-avatar" />;
    } else {
      return <AvatarCircle name={nameFallback} />;
    }
  }
  render() {
    const { avatarImg, type } = this.props,
      info = this.getInfo(),
      avatar = this.getAvatar();

    return (
      <div className={"user-avatar-holder " + type}>
        <span className="avatar">{avatar}</span>
        {info}
      </div>
    );
  }
}

Avatar.propTypes = {
  avatarImg: PropTypes.string.isRequired,
  infoTitle: PropTypes.any,
  nameFallback: PropTypes.string,
  type: PropTypes.string,
};
