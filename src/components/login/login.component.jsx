import React from "react";
import FormInput from "../form-input/form-input.component";
import { signInWithGoogle } from "../../firebase/firebase.utils"
import "./login.styles.css";

class LogIn extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
    };
  }

  handleSubmit = (event) => {
    event.preventDefault();

    this.setState({ email: "", password: "" });
  };

  handleChange = (event) => {
    const { value, name } = event.target; // name, value

    this.setState({ [name]: value });
  };

  // design pattern: controlled input
  render() {
    return (
      <div className="container">
        <div className="sign-in">
          <h2 className="title">Welcome my first React App</h2>

          <form onSubmit={this.handleSubmit}>
            <FormInput
              name="email"
              type="email"
              handleChange={this.handleChange}
              value={this.state.email}
              label="email"
              required
            />
            <FormInput
              name="password"
              type="password"
              handleChange={this.handleChange}
              value={this.state.password}
              label="password"
              required
            />
            <div className="buttons">
              <button type="submit">Sign In</button>
              <button type="button" onClick={signInWithGoogle}>
                Sign in with Google
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default LogIn;
