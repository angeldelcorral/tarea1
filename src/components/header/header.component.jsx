import React from "react";
import { Link, withRouter } from "react-router-dom";
import ExitIcon from "@material-ui/icons/ExitToApp";
import HomeIcon from "@material-ui/icons/Home";
import ListAltIcon from "@material-ui/icons/ListAlt";

import { ReactComponent as Logo } from "../../assets/images/logos/logo.svg";
import "./header.styles.css";

import { auth } from "../../firebase/firebase.utils";

const Header = ({ currentUser, history, match }) => (
  <div className="header">
    <div className="logo_container" to="/">
      <Logo />
      <span>ReactJS</span>
    </div>

    <div className="options">
      {currentUser ? (
        <>
          <p>
            {currentUser.displayName
              ? `Hola, ${currentUser.displayName.split(" ")[0]} `
              : null}
          </p>
          <Link className="option" title="Home" to="/home">
            <HomeIcon />
          </Link>
          <Link className="option" title="Users List" to="users">
            <ListAltIcon />
          </Link>
          <div className="option" onClick={() => logOut(history, match)}>
            <ExitIcon />
          </div>{" "}
        </>
      ) : null}
    </div>
  </div>
);

export default withRouter(Header);

function logOut(history, match) {
  console.log(match.url);
  auth.signOut();
  history.push(match.url);
}
