import React from "react";
import { USER_DATA } from "../../data/users.data";
import { getNamePermissions } from "../../functions/functions.utils";
import Avatar from "../avatar/avatar"
import "./users.styles.css";

export default class UserContent extends React.Component {
  constructor(){
    super()

    this.state = {
      data: USER_DATA,
      originalData: USER_DATA,
    }
  }

  getPermissionsUser(data){
    // console.log(data);
    return data.map((permiso, key) => {
      return (
        <span className="span-item" key={key}>
          {getNamePermissions(permiso)}
        </span>
      );
      })}

  showAlertData(e, data){
    e.preventDefault()
    alert(
      `DATOS DEL USUARIO:\nUsuario: ${data.nombre} ${data.apellido}\nEmail: ${data.email}`
    );
  }

  searchData(e){
    let search = e.target.value.toLowerCase();
    const { originalData } = this.state;
    const filterData = originalData.filter((item) => 
      (item.nombre.toLowerCase().includes(search)) ||
      item.apellido.toLowerCase().includes(search) ||
      item.email.toLowerCase().includes(search))

      this.setState({data: filterData})
  }

  render () {
    const { data } = this.state;

    return (
      <>
        <div className="User-container">
          <div className="filter-field">
            <span>Filtro: 
            <input
              type="text"
              name="search"
              onChange={(e) => this.searchData(e)}
              placeholder="Search by Nombre, Apellido e Email"
              size="50"
            />
            </span>
          </div>
          <div className="Table">
            <table>
              <thead>
                <tr>
                  <th whidth="25%">Nombre</th>
                  <th whidth="25%">Email</th>
                  <th whidth="40%">Permisos</th>
                </tr>
              </thead>
              <tbody>
                {data.map((user, id) => {
                  return (
                    <tr key={id} onClick={(e) => this.showAlertData(e, user)}>
                      <td>
                        <Avatar
                          avatarImg={user.avatar}
                          infoTitle={
                            <strong>{`${user.nombre} ${user.apellido}`}</strong>
                          }
                          nameFallback={
                            user.nombre.charAt(0) + user.apellido.charAt(0)
                          }
                        />
                      </td>
                      <td>{user.email}</td>
                      <td>{this.getPermissionsUser(user.permisos)}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </>
    );
  }
}


// const UsersContent = () => {
//   const getPermissionsUser = (data) => {
//     // console.log(data);
//     return data.map((permiso, key) => {
//       return (
//         <span className="span-item" key={key}>
//           {getNamePermissions(permiso)}
//         </span>
//       );
//     });
//   };

//   const showAlertData = (e, data) => {
//     e.preventDefault()
//     alert(
//       `DATOS DEL USUARIO:\nUsuario: ${data.nombre} ${data.apellido}\nEmail: ${data.email}`
//     );
//   };

//   return (
//     <>
//       <div className="User-container">
//         <div className="Table">
//           <table>
//             <thead>
//               <tr>
//                 <th whidth="25%">Nombre</th>
//                 <th whidth="25%">Email</th>
//                 <th whidth="40%">Permisos</th>

//               </tr>
//             </thead>
//             <tbody>
//               {USER_DATA.map((user, id) => {
//                 return (
//                   <tr key={id} onClick={(e) => showAlertData(e, user)}>
//                     <td>
//                       <Avatar
//                         avatarImg={user.avatar}
//                         infoTitle={<strong>{`${user.nombre} ${user.apellido}`}</strong>}
//                         nameFallback={user.nombre.charAt(0) + user.apellido.charAt(0)}
//                       />
//                     </td>
//                     <td>{user.email}</td>
//                     <td>{getPermissionsUser(user.permisos)}</td>
//                   </tr>
//                 );
//               })}
//             </tbody>
//           </table>
//         </div>
//       </div>
//     </>
//   );
// };

// export default UsersContent;
