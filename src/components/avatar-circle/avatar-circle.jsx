import React from 'react';
import PropTypes from 'prop-types';
import "./avatar-circle.styles.css"

export default class AvatarCircle extends React.Component{
  shouldComponentUpdate(nextProps){
    return nextProps.name !== this.props.name;
  }
  render(){
    const { name } = this.props;
    return(
      <span className="avatar-circle"> { name }</span>
    );
  }
}

AvatarCircle.propTypes = {
  name: PropTypes.string
}