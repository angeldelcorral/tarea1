import React from "react";
import "./home.stiles.css";

import logo from "../assets/images/logos/logo.svg";
function Home() {
  return (
    <div>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Bienvenido a mi primera aplicación React
        </p>
      </header>
    </div>
  );
}

export default Home;
